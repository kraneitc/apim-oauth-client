﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;


namespace SimpleOAuthClientCredGrantWithSecret
{

    /// <summary>
    /// Retrieve an OAuth 2.0 token and call an API endpoint
    /// </summary>
    internal class Program
    {
        private static IConfigurationRoot _configuration;

        private static void Main()
        {
            // Get configuration from app settings file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.nonprod.json", optional: true, reloadOnChange: true);
            _configuration = builder.Build();

            var authorizationServerTokenIssuerUri = new Uri(_configuration.GetSection("ida:authorizationServerTokenIssuerUri").Value);
            var clientId = _configuration.GetSection("ida:clientId").Value;
            var clientSecret = _configuration.GetSection("ida:clientSecret").Value;
            var resource = _configuration.GetSection("refapi:resourceId").Value;
            const string scope = "";

            // Get JWT token
            var rawJwtToken = RequestTokenToAuthorizationServer(
                authorizationServerTokenIssuerUri,
                clientId,
                resource,
                scope,
                clientSecret)
            .GetAwaiter()
            .GetResult();

            // Deserialize JWT token into object
            var authorizationServerToken = JsonConvert.DeserializeObject<AuthorizationServerAnswer>(rawJwtToken);

            // Send API request
            var response = RequestToSecureApi(authorizationServerToken)
                .GetAwaiter()
                .GetResult(); 

            Console.ReadKey();
        }

        /// <summary>
        /// Call API
        /// </summary>
        /// <param name="authorizationServerToken">Authorization Token</param>
        /// <returns>An API request task</returns>
        private static async Task<string> RequestToSecureApi(AuthorizationServerAnswer authorizationServerToken)
        {
            var apiBaseAddress = _configuration.GetSection("refapi:apiBaseAddress").Value;
            var apiEndpoint = _configuration.GetSection("refapi:apiEndPoint").Value;

            HttpResponseMessage responseMessage;

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorizationServerToken.access_token);

                Console.WriteLine($"GET: {apiBaseAddress}{apiEndpoint}");
                Console.WriteLine($"Request Headers - \n{httpClient.DefaultRequestHeaders}");
                var request = new HttpRequestMessage(HttpMethod.Get, $"{apiBaseAddress}{apiEndpoint}");

                responseMessage = await httpClient.SendAsync(request);
                Console.WriteLine($"Response - \n{responseMessage}");
                Console.WriteLine($"\n{await responseMessage.Content.ReadAsStringAsync()}");
            }

            return await responseMessage.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Retrieve OAuth 2.0 Token
        /// </summary>
        /// <param name="uriAuthorizationServer">Authorization server URI</param>
        /// <param name="clientId">GUID identifier of this client</param>
        /// <param name="resource">GUID identifier of the API resource</param>
        /// <param name="scope">Access scope (not required in this scenario)</param>
        /// <param name="clientSecret">Client secret used to authenticate with the Authorization server</param>
        /// <returns></returns>
        private static async Task<string> RequestTokenToAuthorizationServer(
            Uri uriAuthorizationServer, 
            string clientId,
            string resource,
            string scope,
            string clientSecret)
        {
            HttpResponseMessage responseMessage;
            using (var client = new HttpClient())
            {
                var tokenRequest = new HttpRequestMessage(HttpMethod.Post, uriAuthorizationServer);
                var httpContent = new FormUrlEncodedContent(new []
                {
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("resource", resource),
                    new KeyValuePair<string, string>("scope", scope),
                    new KeyValuePair<string, string>("client_secret", clientSecret)
                });

                tokenRequest.Content = httpContent;
                responseMessage = await client.SendAsync(tokenRequest);
            }

            return await responseMessage.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Authorization server answer object
        /// </summary>
        public class AuthorizationServerAnswer
        {
            public string access_token { get; set; }
            public string expires_in { get; set; }
            public string token_type { get; set; }

        }
    }
}
